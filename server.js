const express = require('express');
const app = express();
const port = 3000;
const books = [
	{ title: 'No Logo', author: 'Naomi Klein', pages: 490 },
  	{ title: 'North', author: 'Seamus Heaney', pages: 304},
	{ title: 'To Kill a Mockingbird', author: 'Harper Lee', pages: 281 },
  	{ title: 'The Great Gatsby', author: 'F. Scott Fitzgerald', pages: 180 },
  	{ title: 'Silent Spring', author: 'Rachel Carson', pages: 400 },
  	{ title: 'Noli Me Tángere', author: 'José Rizal', pages: 438 },
  	{ title: '1984', author: 'George Orwell', pages: 328 },
  	{ title: 'The Sixth Extinction', author: 'Elizabeth Kolbert', pages: 391 },
  	{ title: 'Brave New World', author: 'Aldous Huxley', pages: 311 },
  	{ title: 'The Catcher in the Rye', author: 'J.D. Salinger', pages: 234 },
  	{ title: 'The Catcher in the Rye', author: 'J.D. Salinger', pages: 234 },

];

app.listen(port, () => console.log(`server is running at port ${port}`))


app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get('/books/total-pages', (req, res) => {
  
    let pagesArray = []
    // this for loop will append all the page numbers in the pagesArray
    // in preparation for the reduce method
    for (let i = 0; i < books.length; i++){
        pagesArray.push(books[i].pages)
    }

    let pageCount = pagesArray.reduce((x, y) => {
        return x + y
    });
    console.log(pagesArray); //for testing/debugging

    res.send(`Total pages: ${pageCount}`);
});


//CHALLENGE
app.get('/books/large-books', (req, res) => {
    let largeBooks = []

    for (let i = 0; i < books.length; i++){
        if (books[i].pages < 250){
            continue
        } else {
            largeBooks.push(books[i])
        }
    }
    console.log(largeBooks); //for testing/debugging

    res.send(largeBooks);
}); 

//EXTRA CHALLENGE!
app.get('/books/most-prolific-author', (req, res) => {

    let pages = []
    // this loop will append all the pages in the pages array
    for (let i = 0; i < books.length; i++){
        pages.push(books[i].pages)
    }
    
    //to obtain the index of the highest page count
    let maxPageIndex = pages.indexOf(Math.max(...pages))
    
    //for testing/debugging
    console.log(pages)
    console.log(maxPageIndex);
    
    // to acccess the object with the highest page count in the books array
    let prolificAuthor = books[maxPageIndex].author
    let maxPageNumber = books[maxPageIndex].pages

    res.send(`${prolificAuthor} is the most prolific author with ${maxPageNumber} pages written.`)
});

//this are my solutions but I think there are much more effecient and scalable solutions to these. I just can't figure it out for now hehehe!
//currently I am appending the values of the properties of the book objects in a new array. I can't find ways to directly work on the books objects array. 